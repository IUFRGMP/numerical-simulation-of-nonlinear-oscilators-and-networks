//### IMPLEMENTATION OF MODEL SYSTEM               ###//
//###                                              ###//
//### AUTHOR: ERIK GENGEL, University of Potsdam   ###//
//###                                              ###//

#include "../HEADERFILES_PROJECT_DATAGEN/Main_data.h"

void Initializer(vector<double> &y, vector<double> &dydx, double &t, const double yinit, int N){
 //for(int i=0;i<N;i++)
 for(int i=0;i<N;i=i+1)
  {
   //y[2*i] = yinit*cos(double(2*i)/(double(N))*2.0*pi);
   //y[2*i+1] = yinit*sin(double(2*i)/(double(N))*2.0*pi);
   y[i]    = yinit;
   dydx[i] = yinit;//*sin(0.1+double(i)/(double(N))*2.0*pi);
  }
  t=0.0;
// for(int i=0; i<N;i++)
//  {
//   cout << "Initialized: y[" << i << "]=" << y[i] << " dydx[" << i << "]=" << dydx[i] << "\n";
//  }
}

//biological fun ctions for the Morris-Lecar neuron model//

double M_inf(double x)
 {
  return 0.5*(1.0 + tanh((x-V1)/V2));
 }

double W_inf(double x)
 {
  return 0.5*(1.0 + tanh((x-V3)/V4));  
 }

double Lambda(double x)
 {
  return Phi*(cosh(0.5*((x-V3)/V4)));
 }

void sys(vector<double> yy, vector<double> &dyydx, int N, double &t, sys_params &params){
//Noise force
//Stuart-Landau general// eps   Omegaint          
//dyydx[0] = (params.epsin - yy[0]*yy[0] - yy[1]*yy[1])*yy[0] + (params.epsin2ext*(yy[0]*yy[0]+yy[1]*yy[1]) - Omegaint)*yy[1];
//dyydx[1] = (params.epsin - yy[0]*yy[0] - yy[1]*yy[1])*yy[1] + (Omegaint - params.epsin2ext*(yy[0]*yy[0]+yy[1]*yy[1]))*yy[0] + params.extforceamp*(1.0/3.0*cos(params.Omegaext*t)+1.0/3.0*cos(params.Omegaext*sqrt(3.0)/sqrt(5.0)*t+0.25*pi) +1.0/3.0*cos(params.Omegaext*sqrt(2.0)/sqrt(5.0)*t+0.5*pi));//params.extforceamp*cos(params.Omegaext*t);    

//SL other paramterization/other system...
//dyydx[0] = params.epsin*(1.0 - yy[0]*yy[0] - yy[1]*yy[1])*yy[0] - params.epsin2ext*(1.0-yy[0]*yy[0]-yy[1]*yy[1])*yy[1] - Omegaint*yy[1];
//dyydx[1] = params.epsin*(1.0 - yy[0]*yy[0] - yy[1]*yy[1])*yy[1] + Omegaint*yy[0] + params.epsin2ext*(1.0 - yy[0]*yy[0]-yy[1]*yy[1])*yy[0] + params.extforceamp*(1.0/3.0*cos(params.Omegaext*t)+1.0/3.0*cos(params.Omegaext*sqrt(3.0)/sqrt(5.0)*t+0.25*pi) +1.0/3.0*cos(params.Omegaext*sqrt(2.0)/sqrt(5.0)*t+0.5*pi)); 

//Van der Pol//
//dyydx[0] = yy[1];
//dyydx[1] = params.epsin*(1.0-yy[0]*yy[0])*yy[1]-yy[0]+params.extforceamp*(cos(params.Omegaext*t*sqrt(3.0)/sqrt(5.0))+cos(params.Omegaext*t*sqrt(2.0)/sqrt(5.0)+0.25*pi)+cos(params.Omegaext*t/sqrt(5.0)+0.5*pi));//+params.GausianNoise[0];    

//VdP other paramterization //
//double T(((3.0-2.0*log(2.0))/(2.0*pi)*params.epsin*params.epsin+1.0)/(sqrt(1.0+params.epsin*params.epsin)));
//double T(((3.0-2.0*log(2.0))*params.epsin*params.epsin+1.0)/(sqrt(1.0+params.epsin*params.epsin))+(2.0*pi-1.0));
//double T(((3.0-2.0*log(2.0))/(2.0*pi)*params.epsin*params.epsin+4.0*pi)/(sqrt(16.0*pi*pi+params.epsin*params.epsin)));
//double A(T*T), B(T), C(1.0);
//#double A((1.0+0.25*params.epsin)*(1.0+0.25*params.epsin)), B(1.0+0.25*params.epsin), C(4.0);//for use in paper 2
//double A(params.epsin), B(1.0), C(1.0);
//dyydx[0] = yy[1];
//dyydx[1] = params.epsin*B*(1.0-C*yy[0]*yy[0])*yy[1]-A*yy[0]+params.extforceamp*(1.0/3.0*cos(params.Omegaext*t)+1.0/3.0*cos(params.Omegaext*sqrt(3.0)/sqrt(5.0)*t+0.25*pi) +1.0/3.0*cos(params.Omegaext*sqrt(2.0)/sqrt(5.0)*t+0.5*pi));
	
//#dyydx[0] = -A*yy[1];
//#dyydx[1] = (params.epsin*B*(1.0-C*yy[0]*yy[0])*yy[1]+yy[0]+params.extforceamp*(1.0/3.0*cos(params.Omegaext*t)+1.0/3.0*cos(params.Omegaext*sqrt(3.0)/sqrt(5.0)*t+0.25*pi) +1.0/3.0*cos(params.Omegaext*sqrt(2.0)/sqrt(5.0)*t+0.5*pi)));

	//params.extforceamp*(cos(params.Omegaext*t*sqrt(3.0)/sqrt(5.0))+cos(params.Omegaext*t*sqrt(2.0)/sqrt(5.0)+0.25*pi)+cos(params.Omegaext*t/sqrt(5.0)+0.5*pi));//+params.GausianNoise[0];

//Rayleigh//
//dyydx[0] = yy[1];
//dyydx[1] = params.epsin*(1.0-(yy[1]*yy[1]))*yy[1] - yy[0] + params.extforceamp*cos(params.Omegaext*t);// + params.GausianNoise[0]; 

//FitzHugh-Nagumo Iext = 0.5 tau=12.5 
//dyydx[0] = yy[0] - yy[0]*yy[0]*yy[0]/3.0 - yy[1] + params.Iext + params.extforceamp*cos(params.Omegaext*t);// + params.GausianNoise[0];
//dyydx[1] = (yy[0] + params.epsin - params.epsin2ext*yy[1])/params.tau;//Omegaint*yy[0]    

//Morris-Lecar neuron modell//
//dyydx[0] = params.Iext/C*t_re - gL*(yy[0]-VL) - gK*yy[1]*(yy[0] - VK) - gCa*M_inf(yy[0])*(yy[0]-VCa) + params.extforceamp*cos(params.Omegaext*t)/C*t_re;// + params.GausianNoise[0]; //before extfore was sine
//dyydx[1] = Lambda(yy[0])*params.omegas[params.osc]*(W_inf(yy[0])-yy[1]); 

//Rösler oscillator//
//dyydx[0] = -yy[1]-yy[2];
//dyydx[1] = yy[0] + 0.2*yy[1];
//dyydx[2] = 0.6 + yy[2]*(yy[0]-5.7);

//Lorenz oscillator
//dyydx[0] = 10.0*(yy[1]-yy[0]);
//dyydx[1] = yy[0]*(28.0-yy[2])-yy[1];
//dyydx[2] = yy[0]*yy[1] - 8.0/3.0*yy[2];

//Network of SL Oscillators with 
    
/*double Interaction1(0.0), Interaction2(0.0);
for(int k=0; k<N;k += 2)
 {
  //calculate for each oscillator the interaction with all other oscillators
  Interaction1=0.0;
  Interaction2=0.0;
  /*for(int m=0; 2*m<N; m++)//mean diffusive field 
   {
    Interaction1 -= params.coupling[(k*N)/4+m]*(yy[k]-yy[2*m]);
    //Interaction2 -= params.coupling[(k*N)/4+m]*(yy[k+1]-yy[2*m+1]);//only coupling in x
   }//*/
/*  for(int m=0;2*m<N;m++)//
   {
    Interaction1 += params.coupling[k*N/4+m]*(cos(params.lag[k*N/4+m])*yy[2*m] - sin(params.lag[k*N/4+m])*yy[2*m+1]);
    Interaction2 += params.coupling[k*N/4+m]*(cos(params.lag[k*N/4+m])*yy[2*m+1] + sin(params.lag[k*N/4+m])*yy[2*m]);
   }
  //calculate the derivative
  dyydx[k] = (params.epsin - yy[k]*yy[k] - yy[k+1]*yy[k+1])*yy[k] + (params.epsin2ext*(yy[k]*yy[k]+yy[k+1]*yy[k+1]) - Omegaint*params.omegas[k/2] - params.epsin2ext)*yy[k+1] + Interaction1;
  dyydx[k+1] = (params.epsin - yy[k]*yy[k] - yy[k+1]*yy[k+1])*yy[k+1] + (params.epsin2ext + Omegaint*params.omegas[k/2] - params.epsin2ext*(yy[k]*yy[k]+yy[k+1]*yy[k+1]))*yy[k] + Interaction2 + params.extforceamp*cos(params.Omegaext*t);
 }//*/
 
//Network of FHN Oscillators with 
/*double Interaction1(0.0), Interaction2(0.0);
for(int k=0; k<N;k += 2)
 {
  //calculate for each oscillator the interaction with all other oscillators
  Interaction1=0.0;
  Interaction2=0.0;
  for(int m=0; 2*m<N; m++)//mean diffusive field 
   {
    //Interaction1 -= params.coupling[(k*N)/4+m]*(yy[k]-yy[2*m]);//diffusive coupling
    Interaction1 += params.coupling[(k*N)/4+m]*yy[2*m];//additive coupling
    //Interaction2 -= params.coupling[(k*N)/4+m]*(yy[k+1]-yy[2*m+1]);//only coupling in x
   }
  //calculate the derivative
  dyydx[k] = yy[k] - yy[k]*yy[k]*yy[k]/3.0 - yy[k+1] + params.Iext + Interaction1;
  dyydx[k+1] = (yy[k] + params.epsin*params.omegas[k/2] - params.epsin2ext*yy[k+1])/params.tau;
 }//*/

//Network of VdP Oscillators with 
/*double Interaction1(0.0), Interaction2(0.0);
for(int k=0; k<N;k += 2)
 {
  //calculate for each oscillator the interaction with all other oscillators
  Interaction1=0.0;
  Interaction2=0.0;  
  for(int m=0; m<N/2; m++)//mean diffusive field 
   {
    //Interaction1 -= params.coupling[(k*N)/4+m]*(yy[k]-yy[2*m]);//diffusive coupling
    Interaction1 += params.coupling[k/2*params.omegas.size()+m]*yy[2*m];//additive coupling
    //Interaction2 -= params.coupling[(k*N)/4+m]*(yy[k+1]-yy[2*m+1]);//only coupling in x
   }
  //calculate the derivative
  dyydx[k] = yy[k+1];
  dyydx[k+1] = params.epsin*(1.0-yy[k]*yy[k])*yy[k+1]-params.omegas[k/2]*params.omegas[k/2]*yy[k] + Interaction1;
 }//*/
 
//Network of ML Oscillators with 
double Interaction1(0.0), Interaction2(0.0);
for(int k=0; k<N;k += 2)
 {
  //calculate for each oscillator the interaction with all other oscillators
  Interaction1=0.0;
  Interaction2=0.0;  
  for(int m=0; m<N/2; m++)//mean diffusive field 
   {
    //Interaction1 -= params.coupling[(k*N)/4+m]*(yy[k]-yy[2*m]);//diffusive coupling
    //Interaction1 += params.coupling[k/2*params.omegas.size()+m]*yy[2*m];//additive coupling
    Interaction1 += (params.coupling[k/2*params.omegas.size()+m]*(Vrev-yy[k])/(1.0+exp(-(yy[2*m]-Vth)/sigma_syn)))/C*t_re;//waighted synaptic coupling divided by capacity and rescaling factor (see headerfiles constants)
    //Interaction2 -= params.coupling[(k*N)/4+m]*(yy[k+1]-yy[2*m+1]);//only coupling in x
   }
  //calculate the derivative
  dyydx[k] = params.Iext/C*t_re - gL*(yy[k]-VL) - gK*yy[k+1]*(yy[k] - VK) - gCa*M_inf(yy[k])*(yy[k]-VCa) + Interaction1;// + params.GausianNoise[0]; //before extfore was sine; NOTE: Iext now divided: /C*t_re
  dyydx[k+1] = Lambda(yy[k])*params.omegas[k/2]*(W_inf(yy[k])-yy[k+1]); 
 }//*/

//Network of Kuramoto Oscillators with 
/*double Interaction1(0.0), Interaction2(0.0);
for(int k=0; k<N;k += 2)
 {
  //calculate for each oscillator the interaction with all other oscillators
  Interaction1=0.0;
  Interaction2=0.0;  
  for(int m=0; m<N/2; m++)//mean diffusive field 
   {
    //Interaction1 -= params.coupling[(k*N)/4+m]*(yy[k]-yy[2*m]);//diffusive coupling
    Interaction1 += params.coupling[k/2*params.omegas.size()+m]*yy[2*m];//additive coupling
    //Interaction2 -= params.coupling[(k*N)/4+m]*(yy[k+1]-yy[2*m+1]);//only coupling in x
   }
  //calculate the derivative
  dyydx[k] = 
  dyydx[k+1] = 0.0; 
 }//*/
 
}

void sys_unforced(vector<double> yy, vector<double> &dyydx, int N, double &t, sys_params &params){
//Stuart-Landau general//   eps   Omegaint 
//dyydx[0] = (params.epsin - yy[0]*yy[0] - yy[1]*yy[1])*yy[0] + (params.epsin2ext*(yy[0]*yy[0]+yy[1]*yy[1]) - Omegaint*params.omegas[params.osc] - params.epsin2ext)*yy[1];
//dyydx[1] = (params.epsin - yy[0]*yy[0] - yy[1]*yy[1])*yy[1] + (params.epsin2ext + Omegaint*params.omegas[params.osc] - params.epsin2ext*(yy[0]*yy[0]+yy[1]*yy[1]))*yy[0];    

//SL other sytem with better properties
//dyydx[0] = params.epsin*(1.0 - yy[0]*yy[0] - yy[1]*yy[1])*yy[0] - params.epsin2ext*(1.0-yy[0]*yy[0]-yy[1]*yy[1])*yy[1] - Omegaint*yy[1];
//dyydx[1] = params.epsin*(1.0 - yy[0]*yy[0] - yy[1]*yy[1])*yy[1] + Omegaint*yy[0] + params.epsin2ext*(1.0 - yy[0]*yy[0]-yy[1]*yy[1])*yy[0]; 

//Van der Pol//
//dyydx[0] = yy[1];
//dyydx[1] = params.epsin*(1.0-yy[0]*yy[0])*yy[1]-yy[0];    

//double T(((3.0-2.0*log(2.0))/(2.0*pi)*params.epsin*params.epsin+1.0)/(sqrt(1.0+params.epsin*params.epsin)));
//double T(((3.0-2.0*log(2.0))*params.epsin*params.epsin+1.0)/(sqrt(1.0+params.epsin*params.epsin))+(2.0*pi-1.0));
//double T(((3.0-2.0*log(2.0))/(2.0*pi)*params.epsin*params.epsin+4.0*pi)/(sqrt(16.0*pi*pi+params.epsin*params.epsin)));
//double A(T*T), B(T), C(1.0);
//#double A((1.0+0.25*params.epsin)*(1.0+0.25*params.epsin)), B(1.0+0.25*params.epsin), C(4.0);//for use in paper 2
//double A(params.epsin), B(1.0), C(1.0);
//Van der Pol other paramterization//

//dyydx[0] = yy[1];
//dyydx[1] = params.epsin*B*(1.0-C*yy[0]*yy[0])*yy[1]-A*yy[0];

//#dyydx[0] = -A*yy[1];
//#dyydx[1] = (params.epsin*B*(1.0-C*yy[0]*yy[0])*yy[1] + yy[0]);

//Rayleigh//
//dyydx[0] = yy[1];
//dyydx[1] = params.epsin*(1.0-(yy[1]*yy[1]))*yy[1] - yy[0]; 

//FitzHugh-Nagumo Iext = 0.5 tau=12.5 
//dyydx[0] = yy[0] - yy[0]*yy[0]*yy[0]/3.0 - yy[1] + params.Iext;
//dyydx[1] = (yy[0] + params.epsin - params.epsin2ext*yy[1])/params.tau;//Omegaint*yy[0]    

//Morris-Lecar neuron modell//
//dyydx[0] = params.Iext/C*t_re - gL*(yy[0]-VL) - gK*yy[1]*(yy[0] - VK) - gCa*M_inf(yy[0])*(yy[0]-VCa);
//dyydx[1] = Lambda(yy[0])*params.omegas[params.osc]*(W_inf(yy[0])-yy[1]); 

//For network of FHN oscillators 
//dyydx[0] = yy[0] - yy[0]*yy[0]*yy[0]/3.0 - yy[1] + params.Iext;
//dyydx[1] = (yy[0] + params.epsin*params.omegas[params.osc] - params.epsin2ext*yy[1])/params.tau;    

//For network of VdP oscillators 
//dyydx[0] = yy[1];
//dyydx[1] = params.epsin*(1.0-yy[0]*yy[0])*yy[1]-params.omegas[params.osc]*params.omegas[params.osc]*yy[0];   

//Morris-Lecar neuron for networks
 dyydx[0] = params.Iext/C*t_re - gL*(yy[0]-VL) - gK*yy[1]*(yy[0] - VK) - gCa*M_inf(yy[0])*(yy[0]-VCa);
 dyydx[1] = Lambda(yy[0])*params.omegas[params.osc]*(W_inf(yy[0])-yy[1]);    
}

void Henontrick(vector<double> yy, vector<double> &dyydx, int N, double &t,sys_params &params){
//Stuart-Landau general//   eps   Omegaint          
//dyydx[1] = 1.0/((params.epsin - yy[0]*yy[0] - t*t)*t + (Omegaint*params.omegas[params.osc] - params.epsin2ext*(yy[0]*yy[0]+t*t))*yy[0] - params.epsin2ext);
//dyydx[0] = t*dyydx[1];

//SL better
//dyydx[1] = 1.0/(params.epsin*(1.0 - yy[0]*yy[0] - t*t)*t + Omegaint*params.omegas[params.osc]*yy[0] + params.epsin2ext*(1.0 - yy[0]*yy[0]-t*t)*yy[0]);
//dyydx[0] = t*dyydx[1];

//Van der Pol//
//dyydx[1] = 1.0/(params.epsin*(1.0-yy[0]*yy[0])*t-yy[0]);
//dyydx[0] = t*dyydx[1];    

//double T(((3.0-2.0*log(2.0))/(2.0*pi)*params.epsin*params.epsin+1.0)/(sqrt(1.0+params.epsin*params.epsin)));
//double T(((3.0-2.0*log(2.0))*params.epsin*params.epsin+1.0)/(sqrt(1.0+params.epsin*params.epsin))+(2.0*pi-1.0));
//double T(((3.0-2.0*log(2.0))/(2.0*pi)*params.epsin*params.epsin+4.0*pi)/(sqrt(16.0*pi*pi+params.epsin*params.epsin)));
//double A(T*T), B(T), C(1.0);
//#double A((1.0+0.25*params.epsin)*(1.0+0.25*params.epsin)), B(1.0+0.25*params.epsin), C(4.0);//for use in paper 2
//double A(params.epsin), B(1.0), C(1.0);
//Van der Pol other paramterization//
//#dyydx[1] = 1.0/(params.epsin*B*(1.0-C*yy[0]*yy[0])*t+yy[0]);
//#dyydx[0] = -A*t*dyydx[1]; 

//Rayleigh//    
//dyydx[1] = 1.0/(params.epsin*(1.0-(t*t))*t - yy[0]);
//dyydx[0] = t*dyydx[1];
    
//Rayleigh y component//   
//dyydx[0] = 1.0/yy[1];
//dyydx[1] = (params.epsin*(1.0-(yy[1]*yy[1]))*yy[1] - t)*dyydx[0];

//FitzHugh-Nagumo
//dyydx[1] = 1.0/((yy[0] + params.epsin - params.epsin2ext*t)/params.tau);
//dyydx[0] = (yy[0] - yy[0]*yy[0]*yy[0]/3.0 - t + params.Iext)*dyydx[1];

//Morris-Lecar neuron modell//
//dyydx[1] = 1.0/(Lambda(yy[0])*params.omegas[params.osc]*(W_inf(yy[0])-t));
//dyydx[0] = (params.Iext/C*t_re - gL*(yy[0]-VL) - gK*t*(yy[0] - VK) - gCa*M_inf(yy[0])*(yy[0]-VCa))*dyydx[1];

//NETWORK OF FHN 
//dyydx[1] = 1.0/((yy[0] + params.epsin*params.omegas[params.osc] - params.epsin2ext*t)/params.tau);
//dyydx[0] = (yy[0] - yy[0]*yy[0]*yy[0]/3.0 - t + params.Iext)*dyydx[1];

//NETWORK VdP//
//dyydx[1] = 1.0/(params.epsin*(1.0-yy[0]*yy[0])*t-params.omegas[params.osc]*params.omegas[params.osc]*yy[0]);
//dyydx[0] = t*dyydx[1];    

//NETWORK ML
dyydx[1] = 1.0/(Lambda(yy[0])*params.omegas[params.osc]*(W_inf(yy[0])-t));
dyydx[0] = (params.Iext/C*t_re - gL*(yy[0]-VL) - gK*t*(yy[0] - VK) - gCa*M_inf(yy[0])*(yy[0]-VCa))*dyydx[1];
}

void sys_unforced_lin(vector<double> yy, vector<double> &dyydx, int N, double &t, sys_params &params){
//nonlinear equation of SL and eqations for derivatives
//cout << "start sys_lin at t= " << t << "\n";
//cout << "params.osc= " << params.osc << " omega[" << params.osc << "]= " << params.omegas[params.osc] << "\n";
//dyydx[0] = (params.epsin - yy[0]*yy[0] - yy[1]*yy[1])*yy[0] + (params.epsin2ext*(yy[0]*yy[0]+yy[1]*yy[1]) - Omegaint*params.omegas[params.osc] - params.epsin2ext)*yy[1];
//dyydx[1] = (params.epsin - yy[0]*yy[0] - yy[1]*yy[1])*yy[1] + (params.epsin2ext + Omegaint*params.omegas[params.osc] - params.epsin2ext*(yy[0]*yy[0]+yy[1]*yy[1]))*yy[0];  
//dyydx[2] = (-2.0*yy[0]*yy[0]+2.0*params.epsin2ext*yy[0]*yy[1]+(params.epsin-yy[0]*yy[0]-yy[1]*yy[1]))*yy[2]+(-2.0*yy[1]*yy[0]+2.0*params.epsin2ext*yy[1]*yy[1]+(params.epsin2ext*(yy[0]*yy[0]+yy[1]*yy[1])-Omegaint*params.omegas[params.osc]-params.epsin2ext))*yy[3];
//dyydx[3] = (-2.0*yy[0]*yy[1]+(params.epsin2ext + Omegaint*params.omegas[params.osc] - params.epsin2ext*(yy[0]*yy[0]+yy[1]*yy[1]))-2.0*params.epsin2ext*yy[0]*yy[0])*yy[2]+(-2.0*yy[1]*yy[1]-2.0*params.epsin2ext*yy[1]*yy[0]+(params.epsin-yy[0]*yy[0]-yy[1]*yy[1]))*yy[3];  

//VdP
//dyydx[0] = yy[1];
//dyydx[1] = params.epsin*(1.0-yy[0]*yy[0])*yy[1]-yy[0];
//dyydx[2] = yy[3];
//dyydx[3] = (-2.0*params.epsin*yy[0]*yy[1]-1.0)*yy[2] + params.epsin*(1.0-yy[0]*yy[0])*yy[3];

//VdP other parametrization -> const amplitude+omega	
//#double A((1.0+0.25*params.epsin)*(1.0+0.25*params.epsin)), B(1.0+0.25*params.epsin), C(4.0);//for use in paper 2
//#dyydx[0] = -A*yy[1];
//#dyydx[1] = params.epsin*B*(1.0-C*yy[0]*yy[0])*yy[1] + yy[0];
//#dyydx[2] = -A*yy[3];
//#dyydx[3] = (-2.0*params.epsin*B*C*yy[0]*yy[1]+1.0)*yy[2] + params.epsin*B*(1.0-C*yy[0]*yy[0])*yy[3];

//NETWORK nonlinear equations of VdP and eqations for derivaties
//dyydx[0] = yy[1];
//dyydx[1] = params.epsin*(1.0-yy[0]*yy[0])*yy[1]-params.omegas[params.osc]*params.omegas[params.osc]*yy[0];
//dyydx[2] = yy[3];
//dyydx[3] = (-2.0*params.epsin*yy[0]*yy[1]-params.omegas[params.osc]*params.omegas[params.osc])*yy[2] + params.epsin*(1.0-yy[0]*yy[0])*yy[3];

//ML neuronal model network
dyydx[0] = params.Iext/C*t_re - gL*(yy[0]-VL) - gK*yy[1]*(yy[0] - VK) - gCa*M_inf(yy[0])*(yy[0]-VCa);
dyydx[1] = Lambda(yy[0])*params.omegas[params.osc]*(W_inf(yy[0])-yy[1]);
dyydx[2] = (-gL -gK*yy[1] -gCa*M_inf(yy[0]) -gCa*(yy[0]-VCa)/(2.0*V2*cosh((yy[0]-V1)/V2)*cosh((yy[0]-V1)/V2)))*yy[2] + (-gK*(yy[0]-VK))*yy[3];
dyydx[3] = (params.omegas[params.osc]*Phi/(2.0*V4)*sinh((yy[0]-V3)/(2.0*V4))*(W_inf(yy[0])-yy[1])+Lambda(yy[0])*params.omegas[params.osc]/(2.0*V4*cosh((yy[0]-V3)/V4)*cosh((yy[0]-V3)/V4)))*yy[2] + (-Lambda(yy[0])*params.omegas[params.osc])*yy[3];

}

void FindZero(vector<double> &y,double &t, double &ddt, sys_params &params, int N)
{
 double xp(0.0), yp(0.0), tp(0.0);
   do {//foreward integration to find crossing event of poincare section
       xp=y[0]; yp=y[1]; tp=t; //for x and y projections
       rk4var(sys_unforced,y,Nsim,t,N,ddt,params);//integrate unforced system
       //xp=y[0]; yp=y[1]; tp=t;
       //cout << "xp= " << xp << " yp= " << yp << " y[0]= " << y[0] << " y[1]= " << y[1] << " " << tp << "\n";
      }      
   while(!(yp<0.25 && y[1]>0.25));//FOR Mor-Lec
   //while(!(yp<0.0 && y[1]>0.0));//For VdP, Ray, SL
   //while(!(yp>0.0 && y[1]<0.0));//For VdP with other parametrization
   //while(!(yp<0.6 && y[1]>0.6));//For FHN
   //while(!(xp<0.0 && y[0]>0.0));//For Ray if y component is used

   y[1]=tp;  y[0]=xp;//for x projections
   //y[0]=tp;  y[1]=yp;//for y projections
   //double backstep(-xp); //Ray if y projection is used
   //double backstep(-yp); //SL,Ray,VdP
   double backstep(0.25-yp);//Mor-Lec.
   //double backstep(0.6-yp);//FHN
      
   rk4var(Henontrick, y, Nsim, yp, N, backstep,params);//for x projections
   //rk4(Henontrick, y, Nsim, xp, N, backstep,params);//for y projection only for Ray for the moment
   
   t=y[1]; //for x projection
   //t=y[0]; //for y projection
   //y[1]=0.0;//by definition of the Henon Trick, y[1] is zero after backward integration (SL,VdP,Ray)
   y[1]=0.25;//Mor-Lec.
   //y[1]= 0.6; // FHN
   //y[0] = 0.0;//for y projection
}

vector<double> TruePhase(vector<double> ya, double &Period, double &step, int &Nn, double &oldyy, double &counter, double &ddt, int N, sys_params &params, vector<double> dya_ini)
{
  vector<double> p_if(2);
  //vector<double> dya_ini(N);
  double ti(0.0), normder(0.0), scalprod(0.0), freq(0.0), d1(0.0), d2(0.0);//set ti=t if needed
  
  //sys(ya, dya_ini, N, ti, params);//determines derivatives of ya=w=u
  vector<double> ya_lin(2*N);
  for(int k=0;k<N;k++)//fills vector for y and dy
   {
    ya_lin[k] = ya[k];
    ya_lin[N+k] = dya_ini[k];
   }
  //cout << "length ya_lin= " << ya_lin.size() << " values are: " << ya_lin[0] << " " << ya_lin[1] << " " << ya_lin[2] << " " << ya_lin[3] << "\n";
  rk4var(sys_unforced_lin,ya_lin,Nn,ti,2*N,step,params);//Nn is number of relaxation steps, params is paramters, sys_unforced_lin is w,v system
  vector<double> dya(N);
  dya_ini[0]=ya_lin[0]; dya_ini[1]=ya_lin[1];//give only x,y vals
  sys_unforced(dya_ini, dya, N, ti, params);//determines full derivative at limit cycle
  //ti=0.;
  //FindZero(ya_lin,ti,ddt,params,N);//finds the zero crossing for phase calculation (uses dynamics of w only)
  for(int k=0;k<N;k++)
   {  
    d1=dya[k];
    normder += d1*d1;//dya[k]*dya[k];
    d2=ya_lin[N+k]; 
    scalprod += d2*d1;//ya_lin[N+k]*dya[k];//determines frequency using linearized and full derivatives
   }
  //cout << "normder= " << normder << " scalprod= " << scalprod << "\n";

  freq = 2.0*pi/Period*scalprod/normder;

  ti=0.;
  //cout << "before find zero \n";    
  FindZero(dya_ini,ti,ddt,params,N);
  //cout << "after find zero \n";

  p_if[0] = 2.0*pi*(Period-ti)/Period;
  p_if[1] = freq;

  return p_if;
}

vector<double> TruePhase_SL(vector<double> ya, double &Period, double &step, int &Nn, double &oldyy, double &counter, double &ddt, int N, sys_params &params, vector<double> dya_ini)
{
  vector<double> p_if(2);
  double ti(0.0), normder(0.0), scalprod(0.0), freq(0.0), d1(0.0), d2(0.0);//set ti=t if needed

  p_if[0] = atan2(ya[1],ya[0]) - params.epsin2ext*log(sqrt(ya[0]*ya[0]+ya[1]*ya[1]));
  p_if[1] = (dya_ini[1]*(ya[0] - params.epsin2ext*ya[1]) - dya_ini[0]*(ya[1] + params.epsin2ext*ya[0]))/(ya[0]*ya[0]+ya[1]*ya[1]);

  return p_if;
}

//### FOR CALCULATION OF DERIVATIVE FROM HENON NUMERICS BY SAVITZKY GOLAY FILTERING ###//

void copier(vector<double> vec, vector<double> &vec2)
  {
   if(vec.size()!=vec2.size())
    {
     cout << "ERROR: size of vectors is different. To copy:" << vec.size() << " into " << vec2.size() << "\n";
    }
   for(int k=0;k<vec.size();k++)
    {
     vec2[k] = vec[k];
    }
  }

void SawitzkyGolayFilterQ(vector<double> &data2, vector<double> &time, int &WS, int &N, int &M, int d)
 {
  MatD J(WS,M);   //model matrix
  VecD f(M);      //window data
  MatD JJT(WS,WS);//linear matrix J*tranpose(J)
  VecD Jf(WS);    //rhs of least square fit
  VecD coeffs(WS);//solution of least square fit, 

  vector<double> smoothed(data2.size());
  vector<double> smoothed2(data2.size());
  copier(data2, smoothed);

  vector<double> coefficients25(M);
  int index(0), lower(0), upper(smoothed2.size()), shift(0);
  double prod(1.0);

  for(int m=0; m<N; m++)//repetition of smoothings
   {
    for(int k=0; k<smoothed2.size();k++)//all points to smooth or J.cols() cut at ends
     {
      for(int q=0;q<J.cols();q++)
       {
        shift = (-(J.cols()-1)/2+q);
        index = checkIndex(k,lower,upper,shift);
        f(q) = smoothed[index];//set data to smooth
        for(int p=0;p<J.rows();p++)
         {
          prod=1.0;
          for(int r=0;r<p;r++){prod *=(time[index]-time[k]);}//set matrix coeffs
          //J(rows,cols)
          J(p,q) = prod;
         }
       }
      JJT = J*(J.transpose());
      Jf = J*f;
      //cout << J << "\n";
      //cout << f << "\n";
      //cout << JJT <<"\n";
      //cout << Jf << "\n";
      coeffs=JJT.bdcSvd(ComputeThinU | ComputeThinV).solve(Jf);
      //cout << "Convolution result= " << coeffs[0] << " derive= " << coeffs[1] << "\n";
      if(m==N-1){smoothed2[k]=coeffs[d];}
      else{smoothed2[k]=coeffs[0];}
     }
    copier(smoothed2, smoothed);
   }
  copier(smoothed, data2);
 }

void Calc_derivative(vector<double> &derive_Phase, vector<double> &data, vector<double> &proofGrid, int a, int b, int c)
 {
  vector<double> smoothdata(data.size());
    // deg rep    point derive (j,n)
  //int a(12), b(4), c(25), d(1);//d=0 -> phase smooth, d=1 -> phase derivative smooth;
  int d(1);
  copier(data, smoothdata);
  SawitzkyGolayFilterQ(smoothdata, proofGrid, a, b, c, d);
  copier(smoothdata,derive_Phase);
 }//*/

int checkIndex(int &index, int &lower, int &upper, int &shift)
  {
   int newindex(index+shift);
   if(index+shift<lower)
    {
     newindex = (-1.0)*(index+shift);
    }
   if(index+shift>=upper)
    {
     newindex=upper-(index+shift-upper)-1;
    }
   return newindex;
  }
//*/

