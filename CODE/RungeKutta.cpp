//### IMPLEMENTATION OF RK METHODS                 ###//
//###                                              ###//
//### AUTHOR: ERIK GENGEL, University of Potsdam   ###//
//###                                              ###//

#include "../HEADERFILES_PROJECT_DATAGEN/Main_data.h"

void rk1(void (*derives)(vector<double> y, vector<double> &dydx, int N, double &t,  sys_params &params ), vector<double> &y, const double Nsim, double &t, int N, double &ddt,  sys_params &params ){
 //vector<double> dydx1(N);
 for(int j=0; j<Nsim; j++)
  {
   (*derives)(y, params.dydx1,N,t, params);
   for(int i=0; i<N; i++)
    {
     y[i] = y[i] + ddt*params.dydx1[i];
    }
   t = t + ddt;
  }
}

void rk2(void (*derives)(vector<double> y, vector<double> &dydx, int N, double &t,  sys_params &params ), vector<double> &y, const double Nsim, double &t, int N, double &ddt,  sys_params &params ){
 /*vector<double> dydx1(N);
 vector<double> dydx2(N);
 vector<double> y1(N);*/
 for(int j=0; j<Nsim; j++)
  {
   (*derives)(y, params.dydx1,N,t, params);
   for(int i=0; i<N; i++)
    {
     params.y1[i] = y[i] + 0.5*ddt*params.dydx1[i];
    }
   t = t + 0.5*ddt;
   (*derives)(params.y1,params.dydx2,N,t, params);
    for(int i=0; i<N; i++)
    {
     y[i] = y[i] + ddt*params.dydx2[i];
    }
   t = t + 0.5*ddt;
  }
}

void rk3(void (*derives)(vector<double> y, vector<double> &dydx, int N, double &t, sys_params &params ), vector<double> &y, const double Nsim, double &t, int N, double &ddt,  sys_params &params ){
 /*vector<double> dydx1(N);
 vector<double> dydx2(N);
 vector<double> dydx3(N);
 vector<double> y1(N);
 vector<double> y2(N);
 */
 for(int j=0; j<Nsim; j++)
  {
   (*derives)(y, params.dydx1,N,t, params);//k1
   for(int i=0; i<N; i++)
    {
     params.y1[i] = y[i] + 0.5*ddt*params.dydx1[i];//for k2
    }
   t = t + 0.5*ddt;
   (*derives)(params.y1,params.dydx2,N,t, params);//k2
    for(int i=0; i<N; i++)
    {
     params.y2[i] = y[i] + 2.0*ddt*params.dydx2[i] - ddt*params.dydx1[i];//for k3
    }
   t = t + 0.5*ddt;
   (*derives)(params.y2,params.dydx3,N,t, params);
   for(int i=0; i<N; i++)
    {
     y[i] = y[i] + ((ddt/6.0)*params.dydx1[i] + (2.0*ddt/3.0)*params.dydx2[i] + (ddt/6.0)*params.dydx3[i]);
    }
  }
}

void rk4(void (*derives)(vector<double> y, vector<double> &dydx, int N, double &t, sys_params &params), vector<double> &y, const double Nsim, double &t, int N, double &ddt, sys_params &params)
{/*
 vector<double> dydx1(N);
 vector<double> dydx2(N);
 vector<double> dydx3(N);
 vector<double> dydx4(N);
 vector<double> y1(N);
 vector<double> y2(N);
 vector<double> y3(N);
 for(int j=0; j<Nsim; j++)
  {
   (*derives)(y, dydx1,N,t, params);//k1
   for(int i=0; i<N; i++)
    {
     y1[i] = y[i] + 0.5*ddt*dydx1[i];//for k2
    }
   t = t + 0.5*ddt;
   (*derives)(y1,dydx2,N,t, params);//k2
    for(int i=0; i<N; i++)
    {
     y2[i] = y[i] + 0.5*ddt*dydx2[i] ;//for k3
    }
   (*derives)(y2,dydx3,N,t, params);
   for(int i=0; i<N; i++)
    {
     y3[i] = y[i] + ddt*dydx3[i];
    }
   t = t + 0.5*ddt;
   (*derives)(y3,dydx4,N,t, params);
   for(int i=0; i<N; i++)
    {
     y[i] = y[i] + ((ddt/6.0)*dydx1[i] + (ddt/3.0)*dydx2[i] + (ddt/3.0)*dydx3[i] + (ddt/6.0)*dydx4[i]);
    }
  }*/
 for(int j=0; j<Nsim; j++)
  {
   (*derives)(y, params.dydx1,N,t, params);//k1
   for(int i=0; i<N; i++)
    {
     params.y1[i] = y[i] + 0.5*ddt*params.dydx1[i];//for k2
    }
   t = t + 0.5*ddt;
   (*derives)(params.y1,params.dydx2,N,t, params);//k2
    for(int i=0; i<N; i++)
    {
     params.y2[i] = y[i] + 0.5*ddt*params.dydx2[i] ;//for k3
    }
   (*derives)(params.y2,params.dydx3,N,t, params);
   for(int i=0; i<N; i++)
    {
     params.y3[i] = y[i] + ddt*params.dydx3[i];
    }
   t = t + 0.5*ddt;
   (*derives)(params.y3,params.dydx4,N,t, params);
   for(int i=0; i<N; i++)
    {
     y[i] = y[i] + ((ddt/6.0)*params.dydx1[i] + (ddt/3.0)*params.dydx2[i] + (ddt/3.0)*params.dydx3[i] + (ddt/6.0)*params.dydx4[i]);
    }
  }
}

void rk4var(void (*derives)(vector<double> y, vector<double> &dydx, int N, double &t, sys_params &params), vector<double> &y, const double Nsim, double &t, int N, double &ddt, sys_params &params ){
 vector<double> dydx1(N);
 vector<double> dydx2(N);
 vector<double> dydx3(N);
 vector<double> dydx4(N);
 vector<double> y1(N);
 vector<double> y2(N);
 vector<double> y3(N);
 for(int j=0; j<Nsim; j++)
  {
   (*derives)(y, dydx1,N,t, params);//k1
   for(int i=0; i<N; i++)
    {
     y1[i] = y[i] + 0.5*ddt*dydx1[i];//for k2
    }
   t = t + 0.5*ddt;
   (*derives)(y1,dydx2,N,t, params);//k2
    for(int i=0; i<N; i++)
    {
     y2[i] = y[i] + 0.5*ddt*dydx2[i] ;//for k3
    }
   (*derives)(y2,dydx3,N,t, params);
   for(int i=0; i<N; i++)
    {
     y3[i] = y[i] + ddt*dydx3[i];
    }
   t = t + 0.5*ddt;
   (*derives)(y3,dydx4,N,t, params);
   for(int i=0; i<N; i++)
    {
     y[i] = y[i] + ((ddt/6.0)*dydx1[i] + (ddt/3.0)*dydx2[i] + (ddt/3.0)*dydx3[i] + (ddt/6.0)*dydx4[i]);
    }
  }
}

void rk4_stoch(void (*derives)(vector<double> y, vector<double> &dydx, int N, double &t, sys_params &params), vector<double> &y, const double Nsim, double &t, int N, double &ddt, sys_params &params)
{
 params.GausianNoise = noiseGauss(params.noiseamplitudeext, params.sigmaext);
 double eta(sqrt(ddt)*params.GausianNoise[0]), ksi(sqrt(ddt)*(0.5*params.GausianNoise[0]) + 0.5*params.GausianNoise[1]*1.0/(sqrt(3.0)));
 /*vector<double> dydx1(N);
 vector<double> dydx2(N);
 vector<double> dydx3(N);
 vector<double> dydx4(N);
 vector<double> y1(N);
 vector<double> y2(N);
 vector<double> y3(N);
 */
 for(int j=0; j<Nsim; j++)
  {
   (*derives)(y, params.dydx1,N,t,params);
   for(int i=0; i<N; i++)
    {
     params.y1[i] = y[i] + 2.0/3.0*(ddt*params.dydx1[i]);//+2.0/3.0*eta;
    }
   params.y1[1] += 2.0/3.0*eta;
   //params.y1[0] += 2.0/3.0*eta;//FHN
   t = t + 2.0/3.0*ddt;
   
   (*derives)(params.y1,params.dydx2,N,t,params);
   for(int i=0; i<N; i++)
    {
     params.y2[i] = y[i] + 1.5*ddt*params.dydx1[i] -  1.0/3.0*ddt*params.dydx2[i];//+ 2.0/3.0*eta - 2.0/3.0*ksi;
    }
   params.y2[1] += 2.0/3.0*eta - 2.0/3.0*ksi;
   //params.y2[0] += 2.0/3.0*eta - 2.0/3.0*ksi;//FHN
   t = t + ddt*0.5;

   (*derives)(params.y2,params.dydx3,N,t,params);
   for(int i=0; i<N; i++)
    {
     params.y3[i] = y[i] + 7.0/6.0*ddt*params.dydx1[i];// + 2.0/3.0*ddt*ksi;
    }
   params.y3[1] += 2.0/3.0*ddt*ksi;     
   //params.y3[0] += 2.0/3.0*ddt*ksi;//FHN

   (*derives)(params.y3,params.dydx4,N,t,params);
   for(int i=0; i<N; i++)
    {
     y[i] = y[i] + 0.25*ddt*(params.dydx1[i] + 3.0*params.dydx2[i] - 3.0*params.dydx3[i] + 3.0*params.dydx4[i]);// + eta;
    }
   y[1] += eta;
   //y[0] += eta;//FHN
   t = t - 1.0/6.0*ddt;
  }
}

