//### IMPLEMENTATION OF DATA GENERATION            ###//
//###                                              ###//
//### AUTHOR: ERIK GENGEL, University of Potsdam   ###//
//###                                              ###//
 
 #include "../HEADERFILES_PROJECT_DATAGEN/Main_data.h"
 
 int main(int argc, char* argv[])
  {

   //NOTE: also give argv[1] as experiment  number;     
   //defines dynamical systems properties
   sys_params params;

   params.pertin = strtod(argv[2],NULL);                // SL network: mean of coupling strength 
   params.epsin = strtod(argv[3],NULL);                 // SL: amplitude stability, FHN: a-paramter in gating equation
   params.noiseamplitudeext = strtod(argv[4], NULL);    // mean of additive noise to all 2nd components of the network
   params.sigmaext = strtod(argv[5], NULL);             // standart deviation of the same noise
   int Next = strtod(argv[6], NULL);                    // How many simulation steps
   double dtext = strtod(argv[7], NULL);                // time step of simulation
   params.Omegaext = sqrt(5.0)*(strtod(argv[8], NULL)); // external frequency multiplyer
   params.epsin2ext = strtod(argv[9], NULL);            // SL: non-isochronicity paramter, FHN: b-paramter in gating eq.
   int N = 2*int(strtod(argv[10], NULL));               // number of oscillators in the network (only SL)
   params.Iext = strtod(argv[11],NULL);//0.5            // for FHN external stimulation current
   params.tau = strtod(argv[12],NULL);//12.5            // for FHN relaxation time of gating
   params.extforceamp = strtod(argv[13],NULL);          // amplitude of harmonic forcing 
   params.frequencyrange = strtod(argv[14],NULL);       // standart deviation of frequency distribution (only SL)
   params.couplingrange = strtod(argv[15],NULL);        // standart deviation of coupling strength distribution (only SL)
   int mo = int(strtod(argv[16],NULL));                 // modulous, determies which mo'ths time step is printed to file      
   double measure_noise = strtod(argv[17],NULL);        // strength of measurment noise
   int seed = int(strtod(argv[18],NULL));                                  //seed for srand()
   if(N>2)
    {
     for(int k=19; k<19+N/2;k++)//read in as many frequencies as there are oscillators given
      {
       params.omegas.push_back(strtod(argv[k],NULL));
      }
    }
   else
    {
     params.omegas.push_back(Omegaint);
    }//*/
   
   
   double ddt(dtext);//ddt(dt)   NOTE: if read from Constants file comment this in (Time step)
   int NN(Next);     //NN(Ntime) NOTE: if read from Constants file comment this in (number of Points) NOTE: Since 11.2019 roughly the number of Periods
   
   const int nst_a=10000;   
   int osc2_index=1; //next oscillator
   
   
   cout << "START OF DATA GENERATION" << "\n";
   
   //variables
   vector<double> y(N);
   vector<double> dydx(N);
   //scale internal vectors for Runge-Kutta solver
   params.dydx1.resize(N);
   params.dydx2.resize(N);
   params.dydx3.resize(N);
   params.dydx4.resize(N);
   params.y1.resize(N);
   params.y2.resize(N);
   params.y3.resize(N);
   // for smoothing of phi_dot by SG filter
   vector<double> allPhi;                             //stores the time series of all phases
   vector<double> intTime;                            //stores the times 
   vector<double> allPhi_dot;                         //stores the time derivatives of all phases


   double t;
   
   //### SET NETWORK PROPERTIES ###//
   
   //define gausian distributed frequencies of the SL oscillators
   //for PRC determination
   srand(time(NULL)*seed);
   params.GausianNoise.resize(2);
   double GN(0.0);
   double frequencyrange(0.01);
   std::stringstream out1; out1 << "Frequency" << argv[1] << ".dat";
   std::string name1 = out1.str(); ofstream fout1(name1,ios::out);
   fout1.setf(ios::scientific); fout1.precision(15);

   std::stringstream out2; out2 << "Coupling_constants" << argv[1] << ".dat";
   std::string name2 = out2.str(); ofstream fout2(name2,ios::out);
   fout2.setf(ios::scientific); fout2.precision(15);

   cout << "define coupling\n";

   /*for(int k=0;2*k<N;k++)
    {      
     params.GausianNoise = noiseGauss(Omegaint,params.frequencyrange);//NOTE: gausian freq!
     params.omegas.push_back(params.GausianNoise[0]);
    }//*/
   /*for(int k=0;2*k<N;k++)
    {      
     if(k==0)
      {
       params.omegas.push_back(1.0);
       cout << "first nu= " << params.omegas[0] << "\n";
      }
     else
      {
       params.omegas.push_back(params.Omegaext);
       cout << "second nu= " << params.omegas[1] << "\n";   
      }
    }//*/
   for(int k=0; k<params.omegas.size();k++)
    {
     fout1 << params.omegas[k] << "\n"; 
    }
    
   fout1.close();
     
   //define coupling matrix in the network
   for(int k=0;k<N; k=k+2)
    {
     for(int m=0; m<N; m=m+2)
      {
       if((abs(k/2-m/2)==1))//chain
       //if((abs(k/2-m/2)>0))//triangular
        {
         params.GausianNoise = noiseGauss(params.pertin,params.couplingrange);
         GN = params.GausianNoise[0];
        }
       else//avoid self coupling
        {
         GN=0.0;
        }
       params.coupling.push_back(GN);
       fout2 << k/2 << " " << m/2 << " " << GN << "\n";           
      }
    }
   //*/
   
  for(int k=0; k<N;k += 2)
   {
    for(int m=0; 2*m<N; m++)//mean diffusive field 
     {
      cout << "k= " << k << " m= " << m << " coup= " << params.coupling[k/2*params.omegas.size()+m]<< "\n";
     }
   }
   
   for(int k=0;k<N; k=k+2)
    {
     for(int m=0; m<N; m=m+2)
      {
       fout2 << k/2 << " " << m/2 << " " << params.coupling[k/2*params.omegas.size()+m/2] << "\n";           
      }
    }
   fout2.close();
   
   cout << "determine period times \n";

   std::stringstream out0a; out0a << "dataGen" << argv[1] << "." << 0 << ".dat";
   std::string name0a = out0a.str(); ofstream fouta(name0a,ios::out);
   fouta.setf(ios::scientific); fouta.precision(15);
   
   std::stringstream out0b; out0b << "dataGen" << argv[1] << "." << osc2_index << ".dat";
   std::string name0b = out0b.str(); ofstream foutb(name0b,ios::out);
   foutb.setf(ios::scientific); foutb.precision(15);
   
   //### DETERMINE PERIOD TIMES ###//
   Initializer(y, dydx, t, yinit, N);
       
   double tstart(0.0), t1(0.0), T_osc1(0.0), T_osc2(0.0), step_a1(0.0), step_a2(0.0), phi_osc1(0.0), phi_osc2(0.0);
   //### initialize two independent systems for period integration only ###//
   vector<double> yosc1(2); 
   yosc1[0]=y[0]; yosc1[1]=y[1]; 
   vector<double> yosc2(2); 
   if(N>2){yosc2[0]=y[2*osc2_index]; yosc2[1]=y[2*osc2_index+1];}
   else {yosc2[0]=y[0]; yosc2[1]=y[1];}
   
   tstart=t;    //fix starting time for both oscillators
   params.osc=0;                  //choose for first oscillator
   for(int k=0;k<600000;k++)      //do transient for first unperturbed oscillator
    {
     rk4(sys_unforced,yosc1,Nsim,t,2,ddt,params);//unperturbed system
    }   
   cout << "find zero \n";
   for(int k=0; k<50; k++)
    {
     FindZero(yosc1,t,ddt,params,2);       //find first zero crossing
     t1=t;
     FindZero(yosc1,t,ddt,params,2);       //find second zero crossing
     T_osc1=t-t1;                          //calculate Period time on cycle
     step_a1=T_osc1/nst_a;                 //calculate integration step for later phase 
     cout << "T_osc1= " << T_osc1 << "\n";
    }
   if(N>2)//if atleast one more oscillator is present
    {        
     t=tstart; t1=0.0;          //reset time
     params.osc=osc2_index;     //coose second oscillator NOT equation!!!
     for(int k=0;k<600000;k++)  //transient for second unperturbed system
      {
       rk4(sys_unforced,yosc2,Nsim,t,2,ddt,params);//unperturbed system
      }   
     
     for(int k=0; k<50; k++)
      {
       FindZero(yosc2,t,ddt,params,2);       //find first zero crossing
       t1=t;
       FindZero(yosc2,t,ddt,params,2);       //find second zero crossing
       T_osc2=t-t1;                          //calculate Period time on cycle
       step_a2=T_osc2/nst_a;                 //calculate integration step for later phase 
      }    
    }//*/
   int Nn(Nrelax*nst_a);
   
   cout << "integration of system \n";
   double Gnoise1(0.0), Gnoise2(0.0); double noiseamplitudeint(params.noiseamplitudeext); double sigmaint(params.sigmaext);
   
   //### BEGIN ENSEMBLE OR SINGLE OSCILLATOR ###//
   if(N==2)
    {
     //### RE INITIALISATION OF INITIAL CONDITIONS ###//
     Initializer(y, dydx, t, yinit, N);
     
     for(int i=0; i<int(double(NN)/ddt*T_osc1); i++)//integrate forced system for transient NOTE: Ntime -> now Next
      {
       //rk4_stoch(sys,y,Nsim,t,N,ddt,params);//stochastic rk4 if noise is present
       rk4(sys,y,Nsim,t,N,ddt,params);    //classical rk4 if noise is absent
      }
     double yp(t), oldphi1(0.0), oldphi2(0.0), oldyy2(0.0), counter_osc1(0.0), counter_osc2(0.0), phi_d_osc1(0.0);
     //for(int i=0; i<NN; i++)//NOTE: Ntime -> Next //fixed number of points
     //for(int i=0; i<int(double(NN)/ddt*T_osc1); i++)//NOTE: 18.11.2019: set # of periods NN demand that # of integration steps M*dt*omega=NN*2pi rearrange! //fixed number of periods in the unperturbed system
     int i(0);
     while(phi_osc1+2.0*pi*counter_osc1<=double(NN)*2.0*pi+0.5*pi) //fixed number of periods in the perturbed system via phase
      {
        //### MAIN INTEGRATION STEP OF THE WHOLE SYSTEM ###//
        //rk4_stoch(sys,y,Nsim,t,N,ddt,params);//stochastic rk4 f noise is presenta
        rk4(sys,y,Nsim,t,N,ddt,params);    //classical rk4 if noise is absent
        //### Phase for first oscillator ###//
        oldphi1=phi_osc1;
        yosc1[0]=y[0]; yosc1[1]=y[1];
        params.osc=0;
        phi_osc1=TruePhase(yosc1, T_osc1, step_a1, Nn, oldphi1, counter_osc1, ddt, N, params);
	phi_d_osc1=
        //unwrap phase
        if(phi_osc1-oldphi1<=-pi)
         {
          counter_osc1 +=1.0;
          oldphi1=phi_osc1;
         }
        if(phi_osc1-oldphi1>=pi)
         {
          counter_osc1 -=1.0;
         }
      //### For Kernel output ###//
      if(i%mo==0)
       {
        params.GausianNoise = noiseGauss(0.0,measure_noise);
        // all signals paper2
        fouta << t << " " << y[0] << " " << params.Omegaext*t << " " << phi_osc1+2.0*pi*counter_osc1 << " " << y[1] << " " << y[0]/(sqrt(y[0]*y[0]+y[1]*y[1])) << " " << y[1]/(sqrt(y[0]*y[0]+y[1]*y[1])) << " " << y[0]*y[0]*y[0] + y[1] + 2.0*y[0]*y[1] << " " << y[0]*y[0]*y[0]/((y[0]*y[0]+y[1]*y[1])*sqrt(y[0]*y[0]+y[1]*y[1])) + y[1]/(sqrt(y[0]*y[0]+y[1]*y[1])) + 2.0*y[0]*y[1]/(y[0]*y[0]+y[1]*y[1]) << "\n"; 
	
	//other signal
        //fouta << t << " " << y[1] << " " << params.Omegaext*t << " " << phi_osc1+2.0*pi*counter_osc1 << " " << y[0] << "\n";//M5 
        //fouta << t << " " << y[0]*sqrt(params.epsin)/(sqrt(y[0]*y[0]+y[1]*y[1])) << " " << params.Omegaext*t << " " << phi_osc1+2.0*pi*counter_osc1 << " " << y[1]*sqrt(params.epsin)/(sqrt(y[0]*y[0]+y[1]*y[1])) << " " << y[0] << " " << y[1] << "\n";// amplitude constant x and constant y
	//fouta << t << " " << y[0] + y[0]*y[0] - y[0]*y[0]*y[0] + params.GausianNoise[0]  << " " << params.Omegaext*t << " " << phi_osc1+2.0*pi*counter_osc1 << " " << y[1] << "\n";//simple multicomponent only x M7
	    //fouta << t << " " << 0.4*y[0] + 0.3*y[1] + 0.2*y[0]*y[0] + 0.1*y[1]*y[1] + 0.3*y[0]*y[1]  + params.GausianNoise[0]  << " " << params.Omegaext*t << " " << phi_osc1+2.0*pi*counter_osc1 << " " << y[1] << "\n";//simple multicomponent signal M_3 in POBO Book
        //fouta << t << " " << y[0]*(1.0+1.5*sin(y[1])) << " " << params.Omegaext*t << " " << phi_osc1+2.0*pi*counter_osc1 << " " << y[1] << "\n";//critic example M6
	 //  fouta << t << " " << y[0] << " " << params.Omegaext*t << " " << phi_osc1+2.0*pi*counter_osc1 << " " << y[1] << "\n"; //M1
        //fouta << t << " " << 0.4*y[0] + 0.3*y[1] + 0.2*y[0]*y[0] + 0.1*y[1]*y[1] + 0.3*y[1]*y[0] << " " << params.Omegaext*t << " " << phi_osc1+2.0*pi*counter_osc1 << " " << y[1] << "\n"; //M2 for paper 2 to test all ways of phase iterations
	//fouta << t << " " << 0.4*y[0]/(sqrt(y[0]*y[0]+y[1]*y[1])) + 0.3*y[1]/(sqrt(y[0]*y[0]+y[1]*y[1])) + 0.2*y[0]/(sqrt(y[0]*y[0]+y[1]*y[1]))*y[0]/(sqrt(y[0]*y[0]+y[1]*y[1])) + 0.1*y[1]/(sqrt(y[0]*y[0]+y[1]*y[1]))*y[1]/(sqrt(y[0]*y[0]+y[1]*y[1])) + 0.3*y[1]/(sqrt(y[0]*y[0]+y[1]*y[1]))*y[0]/(sqrt(y[0]*y[0]+y[1]*y[1])) << " " << params.Omegaext*t << " " << phi_osc1+2.0*pi*counter_osc1 << " " << y[1] << "\n"; //M2 for paper 2 to test all ways of phase iterations without amplitude modulation!
        //fouta << t << " " << y[0] + 0.5*exp(0.9*y[0])*y[1]*y[0] << " " << params.Omegaext*t << " " << phi_osc1+2.0*pi*counter_osc1 << " " << y[1] << "\n"; // M3 for paper 2 to test all ways of phase iteration
        //fouta << t << " " << y[0] + 0.5*exp(0.9*y[0])*(double(seed)/8.0*y[1]+(1.0-double(seed)/8.0)*y[0])*y[0] << " " << params.Omegaext*t << " " << phi_osc1+2.0*pi*counter_osc1 << " " << y[1] << "\n"; // M_test to estimate the influence of the y component (I.e. variation of the limit cycle) 
        //fouta << t << " " << y[0] + 0.5*exp(0.9*y[0])*(double(seed)/8.0*y[1]+(1.0-double(seed)/8.0)*y[0])*y[0] << " " << params.Omegaext*t << " " << phi_osc1+2.0*pi*counter_osc1 << " "<<  y[1] + 0.5*exp(0.9*y[1])*(double(seed)/8.0*y[0]+(1.0-double(seed)/8.0)*y[1])*y[1] << " " << sqrt(params.epsin)*y[0]/(sqrt(y[0]*y[0]+y[1]*y[1])) + 0.5*exp(0.9*sqrt(params.epsin)*y[0]/(sqrt(y[0]*y[0]+y[1]*y[1])))*(double(seed)/8.0*sqrt(params.epsin)*y[1]/(sqrt(y[0]*y[0]+y[1]*y[1]))+(1.0-double(seed)/8.0)*sqrt(params.epsin)*y[0]/(sqrt(y[0]*y[0]+y[1]*y[1])))*sqrt(params.epsin)*y[0]/(sqrt(y[0]*y[0]+y[1]*y[1])) << " " << sqrt(params.epsin)*y[1]/(sqrt(y[0]*y[0]+y[1]*y[1])) + 0.5*exp(0.9*sqrt(params.epsin)*y[1]/(sqrt(y[0]*y[0]+y[1]*y[1])))*(double(seed)/8.0*sqrt(params.epsin)*y[0]/(sqrt(y[0]*y[0]+y[1]*y[1]))+(1.0-double(seed)/8.0)*sqrt(params.epsin)*y[1]/(sqrt(y[0]*y[0]+y[1]*y[1])))*sqrt(params.epsin)*y[1]/(sqrt(y[0]*y[0]+y[1]*y[1])) << "\n";// M_test, M_test1 (with x and y exchanged) + M_test and M_test1 with amplitude constant x and y
        //fouta << t << " " << y[0] + 0.5*(0.9*y[0] + 0.9*0.9*y[0]*y[0]*0.5+0.9*0.9*0.9*0.9*y[0]*y[0]*y[0]/6.0)*y[0]*y[1] << " " << params.Omegaext*t << " " << phi_osc1+2.0*pi*counter_osc1 << " " << y[1] << "\n"; //finite approx of exp in M3 for paper 2
        //fouta << t << " " << 0.4*y[0] + 0.3*y[1] + 0.2*y[0]*y[0] + 0.1*y[1]*y[1] + params.GausianNoise[0] << " " << params.Omegaext*t << " " << phi_osc1+2.0*pi*counter_osc1 << " " << y[1] << "\n";//for SL
        //fouta << t << " " << 0.4*y[0] + 0.003*y[1] + 0.2*y[0]*y[0] + 0.001*y[1]*y[1] + params.GausianNoise[0] << " " << params.Omegaext*t << " " << phi_osc1+2.0*pi*counter_osc1 << " " << y[1] << "\n";//for VdP
       }
      // play simulation video of the Network
      /*cout << " plot [-"<< sqrt(params.epsin)+0.2 << ":" << sqrt(params.epsin)+0.2 << "][-" << sqrt(params.epsin)+0.2 << ":" << sqrt(params.epsin)+0.2 << "] \'-\' w p ls 7 pointsize 2" << "\n";   
     for(int j=0; j<N; j +=2)                          
	  {
	   cout << y[j] << ' ' << y[j+1] << "\n";    //plot stat of the oscillator j
	  }
      cout<< "e"<<'\n';                               // 2 D Animation*/
      i +=1;
     }
    }//off if N==2
   else
    {
     for(int p=0;p<params.omegas.size();p++)
      {
       cout << "a= " << params.epsin*params.omegas[p] << "\n";
      }
        
     double yp(t), oldphi1(0.0), oldphi2(0.0), oldyy2(0.0), counter_osc1(0.0), counter_osc2(0.0), stop_condition(0.0);
     vector<double> oldphin(params.omegas.size());      //all old phases
     vector<double> phi_oscn(params.omegas.size());     //all true phases
     vector<double> counter_oscn(params.omegas.size()); //all modulos to unwrap the phase
     vector<double> T_oscn(params.omegas.size());       //all separate period times
     vector<double> step_an(params.omegas.size());      //all separate integration steps

     Initializer(y, dydx, t, yinit, N);
     tstart=t;
     for(int i=0; i<int(double(NN)/ddt*T_osc1*0.1); i++)//integrate unforced system for transient NOTE: Ntime -> now Next 
      {
       //rk4_stoch(sys,y,Nsim,t,N,ddt,params);// stochastic rk4 if noise is present
       rk4(sys_unforced,y,Nsim,t,N,ddt,params);    //classical rk4 if noise is absent
      }

     //find periods of all given oscillators
     for(int jj=0;jj<params.omegas.size();jj++)
      {
       yosc1[0]=y[jj];yosc1[1]=y[2*jj+1];
       t=tstart;
       t1=0.0;
       params.osc=jj;
       for(int k=0; k<50; k++)
        {
         FindZero(yosc1,t,ddt,params,2);       //find first zero crossing
         t1=t;
         FindZero(yosc1,t,ddt,params,2);       //find second zero crossing
         T_oscn[jj]=t-t1;                          //calculate Period time on cycle
         step_an[jj]=T_oscn[jj]/nst_a;                 //calculate integration step for later phase 
	}
       cout << "T_oscn["<<jj<<"]= " << T_oscn[jj] << "\n";
      }
     t=tstart;
     //### initialize randomly ###//
     for(int jj=0;jj<params.omegas.size();jj++)
     {
      params.GausianNoise = noiseGauss(0.0,2000.0);
      y[2*jj] = sqrt(params.epsin)*cos(params.GausianNoise[0]); y[2*jj+1]=sqrt(params.epsin)*sin(params.GausianNoise[1]);
      cout << "initial["<<jj<<"] (x,y)=("<<y[jj]<<","<<y[2*jj+1]<<"\n";
     }
    T_osc1=0.0;
    for(int ll=0; ll<T_oscn.size();ll++)
     {
      if(T_oscn[ll]>T_osc1){T_osc1=T_oscn[ll];}
     }

     cout << "transient \n";
    for(int i=0; i<int(double(NN)/ddt*T_osc1); i++)//integrate forced system for transient NOTE: Ntime -> now Next
     {
      //rk4_stoch(sys,y,Nsim,t,N,ddt,params);// stochastic rk4 if noise is present
      rk4(sys,y,Nsim,t,N,ddt,params);    //classical rk4 if noise is absent
     }

     //for(int i=0; i<int(double(NN)/ddt*T_osc1); i++)//NOTE: Ntime -> Next NOTE: This approach only works in uncoupled regime
    cout << "main integration \n";
    int i(0);
    while(stop_condition==0.0)
     {
//### MAIN INTEGRATION STEP OF THE WHOLE SYSTEM ###//
        //rk4_stoch(sys,y,Nsim,t,N,ddt,params); //stochastic rk4 if noise is present
        rk4(sys,y,Nsim,t,N,ddt,params);     //classical rk4 if noise is absent
      	//phases for all n oscillators 
	oldphin=phi_oscn;
	 //cout << "phases and oscillator coordinates \n";
	 //for(int jj=0;jj<params.omegas.size();jj++)
	 //{
	 //  cout << "i= " << i << " " << oldphin[jj] << " coords (x,y)=(" <<y[2*jj]<< ","<< y[2*jj+1]<< ")\n";
	 // } 
     //    cout << "### \n";

	 for(int jj=0;jj<params.omegas.size();jj++)
          {
           yosc1[0]=y[2*jj]; yosc1[1]=y[2*jj+1];
           params.osc=jj;
           phi_oscn[jj]=TruePhase(yosc1, T_oscn[jj], step_an[jj], Nn, oldphin[jj], counter_oscn[jj], ddt, N, params);
           //unwrap phase
           if(phi_oscn[jj]-oldphin[jj]<=-pi)
            {
             counter_oscn[jj] +=1.0;
             oldphin[jj]=phi_oscn[jj];
            }
           if(phi_oscn[jj]-oldphin[jj]>=pi)
            {
             counter_oscn[jj] -=1.0;
            }
	  }
	  
	  //cout << t << " " << y[0] << " " << params.Omegaext*t << " " << phi_osc1+2.0*pi*counter_osc1 << " " << y[1] << "\n"; //M1
	  
	 if(i%mo==0)
          {
           foutb << t << " " ;
	   intTime.push_back(t);//store all time points
	   for(int jj=0;jj<params.omegas.size();jj++)
	    {
         foutb <<y[jj*2] << " " ;
	    }
	   foutb << params.Omegaext*t << " " ;
	   for(int jj=0;jj<params.omegas.size();jj++)
	    {
             foutb << phi_oscn[jj] +2.0*pi*counter_oscn[jj] << " " ;
             //cout << "[i,j]= " << i << " , " << jj << " " << phi_oscn[jj] + 2.0*pi*counter_oscn[jj] << "\n";
	     allPhi.push_back(phi_oscn[jj] +2.0*pi*counter_oscn[jj]);//store all phases
	     if(phi_oscn[jj] +2.0*pi*counter_oscn[jj]>=double(NN)*2.0*pi+pi)
	      {
	       stop_condition=1.0;
	       for(int kk=0;kk<params.omegas.size();kk++)
	     	{
             if(phi_oscn[kk] +2.0*pi*counter_oscn[kk]<double(NN)*2.0*pi+pi)  {stop_condition=0.0;}
		    }//recheck if any oscillator still is below the desired period number
	      }//stop the integration if (NN +0.5) periods
	    }
	   for(int jj=0;jj<params.omegas.size();jj++)
	    {
             foutb << y[2*jj+1] << " " ;
	    }
	   foutb << "\n";
	  }
        //cout << "step " << i << "\n";
      //### For Kernel output ###//
      //   if(i%mo==0)
        //  {
        //params.GausianNoise = noiseGauss(0.0,measure_noise);
        //fouta << t << " " << 0.4*y[0] + 0.3*y[1] + 0.2*y[0]*y[0] + 0.1*y[1]*y[1] + params.GausianNoise[0] << " " << params.Omegaext*t << " " << phi_osc1+2.0*pi*counter_osc1 << " " << y[1] << "\n";
       //    fouta << t << " " << y[0] << " " << params.Omegaext*t << " " << phi_osc1+2.0*pi*counter_osc1 << " " << y[1] << "\n";
       //    foutb << t << " " <<y[osc2_index*2] << " " << params.Omegaext*t << " " << phi_osc2 +2.0*pi*counter_osc2<< " " << y[osc2_index*2+1] << "\n";
        //  }
      // use this for DAT_ITHT_KER, since assumtion is to know the phase of the forcing   << Omega*t << " " << y[1] <<  "\n";

      // play simulation video of the Network
      /*cout << " plot [-"<< sqrt(params.epsin)+0.2 << ":" << sqrt(params.epsin)+0.2 << "][-" << sqrt(params.epsin)+0.2 << ":" << sqrt(params.epsin)+0.2 << "] \'-\' w p ls 7 pointsize 2" << "\n";   
     for(int j=0; j<N; j +=2)                          
          {
           cout << y[j] << ' ' << y[j+1] << "\n";    //plot stat of the oscillator j
          }
      cout<< "e"<<'\n';                               // 2 D Animation*/
      i+=1;
     }
    }
   fouta.close();
   foutb.close();

   //### Smooth calculation of derivative using SG filter ###//
   
   std::stringstream fs; fs << "phi_freq" << argv[1] << ".dat";
   std::string nfs = fs.str(); ofstream ffs(nfs,ios::out);
   ffs.setf(ios::scientific); ffs.precision(15);
   
   cout << " calculation of derivatives \n";
   vector<double> all_derivPhi(allPhi.size());
   for(int k=0; k<params.omegas.size(); k++)
    {
     //every phase time series
     vector<double> one_phi;
     int j(0);
     for(int l=k; l<allPhi.size(); l=l+params.omegas.size())
      {
       one_phi.push_back(allPhi[l]);
       //cout << allPhi[l] << " " << intTime[j] << "\n" ;
       j +=1;
      }
      
     /*for(int m=0; m<one_phi.size(); m++)
      {
       cout << intTime[m] << " " << one_phi[m] << "\n";
      }*/
      
     cout << "\n";
     vector<double> derive_Phase(one_phi.size());
     //cout << "time.s= " << intTime.size() << " one_phi.s= " << one_phi.size() << " derive.phi.s= " << derive_Phase.size() << " all_derive_phi.s= " << all_derivPhi.size() << "\n";
     int a(12), b(4), c(25);//degree,repeat,window
     Calc_derivative(derive_Phase, one_phi, intTime, a, b, c);
     for(int l=0; l<one_phi.size(); l++)//store for oscillator k the derivative
      {
       all_derivPhi[k*one_phi.size()+l] = derive_Phase[l];
       //cout << derive_Phase[l] << " " << intTime[l] << "\n";
      }
    }

   /*cout << "controle output allPhi and all_derivPhi: \n";
   for(int k=0;k<allPhi.size();k++)
    {
     //cout << k << " " << allPhi[k] << " " << all_derivPhi[k] << "\n";
    }*/
    
  
   for(int l=0; l<intTime.size();l++)
    {
     ffs << intTime[l] << " ";
     for(int k=0; k<params.omegas.size(); k++)//write out phi and then phi_dot
      {
        ffs << allPhi[k+l*params.omegas.size()] << " " << all_derivPhi[intTime.size()*k+l] << " ";
     //   cout << allPhi[l+k*params.omegas.size()] << " " << all_derivPhi[intTime.size()*l+k] << "\n";
      }
     ffs << "\n";
    }
   ffs.close();//*/
  
   cout << "finished \n";   
   return 0;
  }
