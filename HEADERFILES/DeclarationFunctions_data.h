// ### FUNCTION DECLARATION FOR DATA GENERATION       ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //

#include "baseInclude.h"

//### DECLARATIONS IN CHRONOLOGICAL ORDER OF THE CODE ###//

vector<double> noiseGauss(double mean, double sigma);
void Initializer(vector<double> &y, vector<double> &dydx, double &t, const double yinit, int N);
void sys(vector<double> yy, vector<double> &dyydx, int N, double &t, sys_params &params);
void sys_unforced(vector<double> yy, vector<double> &dyydx, int N, double &t, sys_params &params);
void Henontrick(vector<double> yy, vector<double> &dyydx, int N, double &t, sys_params &params);
void FindZero(vector<double> &y,double &t, double &ddt, sys_params &params, int N);
vector<double> TruePhase(vector<double> ya, double &Period, double &step, int &Nn, double &oldyy, double &counter, double &ddt, int N, sys_params &params, vector<double> dya_ini);
vector<double> TruePhase_SL(vector<double> ya, double &Period, double &step, int &Nn, double &oldyy, double &counter, double &ddt, int N, sys_params &params, vector<double> dya_ini);
void linearFit(double &m, double &n, vector<double> &data, vector<double> &timeGrid, int &start, int &end, double &stepadjust);
void sys_unforced_lin(vector<double> yy, vector<double> &dyydx, int N, double &t, sys_params &params);

//biological functions for the Morris-Lecar neuron model
double M_inf(double x);
double W_inf(double x);
double Lambda(double x);

//for smooth phase derivative calculation by SG filter
int checkIndex(int &index, int &lower, int &upper, int &shift);
void copier(vector<double> vec, vector<double> &vec2);
void SawitzkyGolayFilterQ(vector<double> &data2, vector<double> &time, int &WS, int &N, int &M, int d);
void Calc_derivative(vector<double> &derive_Phase, vector<double> &data, vector<double> &proofGrid, int a, int b, int c);







