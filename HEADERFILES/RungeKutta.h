// ### RK FUNCTION DECLARATION FOR DATA GENERATION    ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //

#include "baseInclude.h"

void rk1(void (*derives)(vector<double> y, vector<double> &dydx, int N, double &t, sys_params &params), vector<double> &y, const double Nsim, double &t, int N, double &ddt, sys_params &params);

void rk2(void (*derives)(vector<double> y, vector<double> &dydx, int N, double &t, sys_params &params), vector<double> &y, const double Nsim, double &t, int N, double &ddt, sys_params &params);

void rk3(void (*derives)(vector<double> y, vector<double> &dydx, int N, double &t, sys_params &params), vector<double> &y, const double Nsim, double &t, int N, double &ddt, sys_params &params);

void rk4var(void (*derives)(vector<double> y, vector<double> &dydx, int N, double &t, sys_params &params), vector<double> &y, const double Nsim, double &t, int N, double &ddt, sys_params &params);

void rk4(void (*derives)(vector<double> y, vector<double> &dydx, int N, double &t, sys_params &params), vector<double> &y, const double Nsim, double &t, int N, double &ddt, sys_params &params);

void rk4_stoch(void (*derives)(vector<double> y, vector<double> &dydx, int N, double &t, sys_params &params), vector<double> &y, const double Nsim, double &t, int N, double &ddt, sys_params &params);

