// ### CONSTANT DEFINITIONS DATA GENERATION           ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //

#include "baseInclude.h"

//const int N(2);
const double Nsim(1);
const double Ntime(300000);        //number of time steps
const double Tbegin(0.0);         //time ofset
const double Tend(0.0);           //endtime (not in use?)
const double dt(0.02);            //time step
const double yinit(1.2);          //x,y initial value for SL,VdP Ray -> before 1.2
const double eps(10.0);           //given as argv (nonlinearity parameter, friction)
const double epsin2(0.0);         //non-isochrinicity parameter for SL system
const double Omega(sqrt(5.0));//sqrt(3.0));//frequency of external forcing
const double Omegaint(1.0);       //frequency of internal oscillation SL oscilator
const double pert(0.01);          //given as argv (perturbation strength of deterministic forcing)
const double noiseamplitude(0.0); //mean deviation (offset) of gausian statistical force 
const double sigma(0.001);         //amplitude of statistical force component /standart deviation
const int Nrelax = 3;             //number of relaxations for determination of asymptotic phase

//### PARAMETERS FOR NEURONAL MODELS ###//

// MORRIS LECAR //
// MORRIS LECAR //
//Paramters from "Bifurcations in Morris–Lecar neuron model"
//               "FORCED SYNCHRONIZATION IN MORRIS–LECAR NEURONS"
//TYPEI
const double t_re(20.0);//(1.0);
const double C(20.0);
const double I(50.0/C*t_re);//#in true units: 50mV
const double gL(2.0/C*t_re);//#
const double gK(8.0/C*t_re);//#
const double gCa(4.0/C*t_re);//#
const double V1(-1.2);//#
const double V2(18.0);//#
const double V3(12.0);//###### -> class 1
const double V4(17.4);//#
const double VL(-60.0);//#
const double VK(-80.0);//#
const double VCa(120.0);//#
const double Phi (1.0/15.0*t_re);//#
//*/
// Synaptic coupling parameters for network:  (Thesis Rok Chestnik)
const double Vrev(20.0);//in Thesis everything divided by 100
const double Vth(25.0);
const double sigma_syn(1.0);
//
//

//TYPE II "Comparison of Coding Capabilities of Type I and Type II Neurons 2004"
/*const double I(0.135);
const double gL(0.5);
const double gK(2.0);
const double gCa(1.1);
const double V1(-0.01);
const double V2(0.15);
const double V3(0.0167);
const double V4(0.25);
const double VL(-0.5);
const double VK(-0.7);
const double VCa(1.7);
const double Phi (0.2);
//*/
//TYPEI
/*const double I(0.0718);
const double gL(0.5);
const double gK(2.0);
const double gCa(1.0);
const double V1(-0.01);
const double V2(0.15);
const double V3(0.1);
const double V4(0.145);
const double VL(-0.5);
const double VK(-0.7);
const double VCa(1.0);
const double Phi (0.333);
//*/
//TYPEII 
/*const double I(0.05);//works
const double gL(0.5);
const double gK(2.0);
const double gCa(1.1);
const double V1(-0.01);
const double V2(0.21);
const double V3(-0.04);
const double V4(0.25);
const double VL(-0.5);
const double VK(-0.7);
const double VCa(1.7);
const double Phi (0.2);
//*/
//TYPE I
/*const double I(0.0718);
const double gL(0.5);
const double gK(2.0);
const double gCa(1.0);
const double V1(-0.01);
const double V2(0.15);
const double V3(0.1);
const double V4(0.145);
const double VL(-0.5);
const double VK(-0.7);
const double VCa(1.0);
const double Phi(0.333);
//*/



