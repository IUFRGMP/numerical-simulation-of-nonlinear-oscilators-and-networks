// ### MAIN HEADER FOR DATA GENERATION                ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //

//### BASIC SELF-DEFINED HEADERS ###//
#include "Constants_data.h"

typedef struct sys_params
 {
  //external forcing parameters
  double pertin;
  double Omegaext;
  double extforceamp;//only for external forcings
  //stability of limit cycle and nonisochronicity parameter SL system
  double epsin;
  double epsin2ext;
  // relaxation time and external current FitzHugh-Naguma neuron
  double Iext;
  double tau;
  //distribution vector of oscillators natural frequency (SL), coupling properties
  vector<double> omegas;   //natural frequencies
  vector<double> coupling; //coupling matrix
  vector<double> lag;      //phase lag paramter matrix
  double frequencyrange;
  double couplingrange;
  //two indices for the calculation of asymptotic phases
  int osc;
  //noise parameters
  double noiseamplitudeext;
  double sigmaext;
  //define noise vector for Runge-Kutta internal solving
  vector<double> GausianNoise;
  //internal Runge-Kutta vectors for simulation
  vector<double> dydx1;
  vector<double> dydx2;
  vector<double> dydx3;
  vector<double> dydx4;
  vector<double> y1;
  vector<double> y2;
  vector<double> y3;
 }sys_params;

#include "RungeKutta.h"
#include "DeclarationFunctions_data.h"
#include "eigen/Eigen/Dense"        //for linear algebra 

using namespace Eigen;

typedef Matrix<double,Dynamic,Dynamic> MatD;
typedef Matrix<double,Dynamic,1> VecD;

const double pi(atan(1.0)*4.0); //pi

//#define short_transient //comment in for point cloude of short transients in SL network if out: smooth tranjectory
#define generic_true_phase //comment in for numeric construction of phase and inst. frequency from relaxation. if out: only SL!
